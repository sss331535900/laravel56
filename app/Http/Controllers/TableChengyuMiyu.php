<?php


namespace App\Http\Controllers;


use App\Chengyu;
use App\ChengyuMiyu;
use Illuminate\Http\Request;

/**
 * system login name Administrator
 * Autor             LAOSONG
 * Time              2019/9/5 14:08
 * IDE name          PhpStorm
 * project name      laravel5.6
 */
class TableChengyuMiyu extends Controller
{
    public function getinfo(Request $request){
         $input=$request->input();
         $info=ChengyuMiyu::getInfos($input);
         $chengyuInfo=Chengyu::Relation($info,'cyid','id',$select="id,title",$tableasli="chengyu_");
         return $chengyuInfo;
    }
}